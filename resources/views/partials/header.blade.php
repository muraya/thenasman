<?php
/**
 * Created by PhpStorm.
 * User: muraya
 * Date: 3/24/19
 * Time: 11:20 AM
 */

?>
<header class="header">
    <div class="header-top bg-theme-colored sm-text-center">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-md-8">
                    <div class="widget no-border m-0">
                        <ul class="list-inline sm-text-center mt-5">
                            <li class="hidden-xs">
                                <a href="#" class="text-white">FAQ</a>
                            </li>
                            <li class="text-white hidden-xs">|</li>
                            <li class="hidden-xs">
                                <a href="#" class="text-white">Help Desk</a>
                            </li>
                            <li class="text-white hidden-xs">|</li>
                            <li>
                                <a href="#" class="text-white">Support</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-6 col-md-4">
                    <div class="widget no-border m-0">
                        <ul class="styled-icons icon-dark icon-circled icon-theme-colored icon-sm pull-right sm-pull-none sm-text-center">
                            <li><a href="#"><i class="fa fa-facebook text-white"></i></a></li>
                            <li class="hidden-xs"><a href="#"><i class="fa fa-twitter text-white"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram text-white"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin text-white"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-middle p-0 bg-lightest xs-text-center">
        <div class="container pt-0 pb-0">
            <div class="row">
                <div class="col-xs-6 col-sm-4 col-md-5">
                    <div class="widget no-border m-0">
                        <a class="menuzord-brand xs-pull-center mb-15" href="javascript:void(0)"><img src="images/logo-wide.png" alt=""></a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4">
                    <div class="widget no-border m-0">
                        <div class="mt-10 mb-10 text-right sm-text-center">
                            <div class="font-15 text-black-333 text-uppercase mb-5 font-weight-600"><i class="fa fa-phone-square text-theme-colored font-24"></i> +(012) 345 6789</div>
                            <a class="font-12 text-gray" href="#">Call us for more details!</a>
                        </div>
                    </div>
                </div>
                <div class="hidden-xs col-sm-4 col-md-3">
                    <div class="widget no-border m-0">
                        <div class="mt-10 mb-10 text-right sm-text-center">
                            <div class="font-20 text-black-333 text-uppercase mb-5 font-weight-600"><i class="fa fa-envelope text-theme-colored font-24"></i> Mail us today</div>
                            <a class="font-12 text-gray" href="#"> info@yourdomain.com</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-nav">
        <div class="header-nav-wrapper navbar-scrolltofixed bg-light" style="z-index: 1000;">
            <div class="container">
                <nav id="menuzord" class="menuzord default bg-light menuzord-responsive">
                    {{--<a href="javascript:void(0)" class="showhide" style="display: none;"><em></em><em></em><em></em></a>--}}
                    <ul class="menuzord-menu menuzord-indented scrollable" style="max-height: 400px; display: block;">
                        <li class="active"><a href="#home">Home</a>
                        </li>
                        <li><a href="#">Services<span class="indicator"></span></a>
                            <ul class="dropdown" style="right: auto;">
                                <li><a href="page-services-relationship-problems.html">Relationship Problems</a></li>
                                <li><a href="page-services-depression-treatment.html">Depression Treatment</a></li>
                                <li><a href="page-services-sexual-counselling.html">Sexual Counselling</a></li>
                                <li><a href="page-services-personal-development.html">Personal Development</a></li>
                                <li><a href="page-services-couples-counselling.html">Couples Counselling</a></li>
                                <li><a href="page-services-anxiety-counselling.html">Anxiety Counselling</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Booking</a></li>
                        <li><a href="#">Membership</a> </li>
                        <li><a href="#">Blog</a> </li>
                        <li><a href="#">Change Works</a> </li>
                        <li><a href="#">Contacts</a> </li>

                        <li class="scrollable-fix"></li></ul>
                    <ul class="pull-right hidden-sm hidden-xs">
                        <li>
                            <a class="btn btn-colored btn-flat btn-theme-colored mt-15 ajaxload-popup" href="#"> Book Now</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div><div></div>
    </div>
</header>
